//
//  ViewController.swift
//  MoMentor
//
//  Created by Filbert Hartawan on 26/03/19.
//  Copyright © 2019 AppleDeveloperAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameTextField.setPadding()
        passwordTextField.setPadding()
        usernameTextField.bottomBorder()
    }


}

