//
//  SignUpViewController.swift
//  MoMentor
//
//  Created by Filbert Hartawan on 27/03/19.
//  Copyright © 2019 AppleDeveloperAcademy. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repeatPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameTextField.setPadding()
        usernameTextField.bottomBorder()
        passwordTextField.setPadding()
        passwordTextField.bottomBorder()
        repeatPasswordTextField.setPadding()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
