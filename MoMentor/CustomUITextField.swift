//
//  CustomUITextField.swift
//  MoMentor
//
//  Created by Filbert Hartawan on 27/03/19.
//  Copyright © 2019 AppleDeveloperAcademy. All rights reserved.
//

import UIKit

extension UITextField{
    
    func setPadding(){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height:self.frame.height))
        self.leftView = paddingView
        self.rightView = paddingView
        self.leftViewMode = .always
        self.rightViewMode = .always
    }
    
    func bottomBorder(){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.white.withAlphaComponent(0.5).cgColor
        border.frame = CGRect(x: 25, y: self.frame.size.height - width, width:  self.frame.size.width-50, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
